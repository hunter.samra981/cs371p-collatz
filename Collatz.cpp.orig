// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert
#include <iostream> // cin, cout, endl

#include "Collatz.hpp"

// ------------
// collatz_eval
// ------------

int solve_collatz(int val1, int val2) {
    int max_count = 0;
    long cache[100000] = {};

    // A cache of all the times there is a new maximum value when going from 1 to 1mil
    int eagerCacheKeys[] = {1, 2, 3, 6, 7, 9, 18, 25, 27, 54, 73, 97, 129, 171, 231, 313, 327, 649, 703, 871, 1161, 2223, 2463, 2919, 3711, 6171, 10971, 13255, 17647, 23529, 26623, 34239, 35655, 52527, 77031, 106239, 142587, 156159, 216367, 230631, 410011, 511935, 626331, 837799};
    int eagerCacheVals[] = {1, 2, 8, 9, 17, 20, 21, 24, 112, 113, 116, 119, 122, 125, 128, 131, 144, 145, 171, 179, 182, 183, 209, 217, 238, 262, 268, 276, 279, 282, 308, 311, 324, 340, 351, 354, 375, 383, 386, 443, 449, 470, 509, 525};
    
    int start_range = val1 < val2 ? val1 : val2;
    int end_range = val1 < val2 ? val2 : val1;

    // Halving the number of calculations in some cases
    int potential_end = (val2 / 2) + 1;
    if (potential_end > val1) {
        start_range = potential_end;
    }
    
    // Use the eager cache
    for(int i = 0; i < sizeof(eagerCacheKeys)/sizeof(eagerCacheKeys[0]); ++i) {
        if(eagerCacheKeys[i] >= start_range && eagerCacheKeys[i] <= end_range) {
            max_count = eagerCacheVals[i];
        }
    }
    if(max_count > 0) {
        return max_count;
    }

    for(int i = start_range; i <= end_range; ++i) {
        long num_in_sequence = i;
        int count = 1;

        //set up the cache "storing" funtionality
        long cache_index[525] = {};
        int cycle_index = 0;

        while(num_in_sequence > 1) {
            if(num_in_sequence <= 100000) {
                // We've seen this number before, just do a lookup
                if(cache[num_in_sequence-1] > 0) {
                    count += cache[num_in_sequence-1]-1;
                    break;
                }
                // New numer! let's store it for later
                cache[num_in_sequence-1] = count*(-1);
                cache_index[cycle_index] = num_in_sequence;
                ++cycle_index;
            }
            // (n/2) operation
            if(num_in_sequence % 2 == 0) {
                num_in_sequence = num_in_sequence / 2;
                ++count;
            }

            // (n*3)+1 operation
            else {
                num_in_sequence = num_in_sequence + (num_in_sequence >> 1) + 1;
                ++++count;
            }
        }
        if(count > max_count) {
            max_count = count;
        }

        //Updating all new values we found to cache
        for(int i = 0; i < sizeof(cache_index)/sizeof(cache_index[0]); ++i) {
            if(cache_index[i] <= 0) {
                break;
            }
            int num_to_cache = cache_index[i];
            cache[num_to_cache-1] = (long)count + cache[num_to_cache-1]+1;
        }
        
    }
    return max_count;
}

tuple_type_2 collatz_eval (const tuple_type_1& t1) {
	auto [i, j] = t1;
    assert(i > 0);
    assert(i <= 1000000);

    assert(j > 0);
    assert(j <= 1000000);
    
    int v = solve_collatz(i,j);
    assert(v >= 0);
    return {i, j, v};}

