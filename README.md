# CS371p: Object-Oriented Programming Collatz Repo

* Name: Hunter Samra

* EID: hgs567

* GitLab ID: hunter.samra981

* HackerRank ID: hunter_samra98

* Git SHA: 66e8961d875e2e3cab0181c58e5f19dec615af0a

* GitLab Pipelines: https://gitlab.com/hunter.samra981/cs371p-collatz/-/pipelines/995263679/builds

* Estimated completion time: 10

* Actual completion time: 4

* Comments: N/A
