// -----------
// Collatz.hpp
// -----------

#ifndef Collatz_hpp
#define Collatz_hpp

// --------
// includes
// --------

#include <tuple> // tuple

using namespace std;

// ------
// usings
// ------

using tuple_type_1 = tuple<int, int>;
using tuple_type_2 = tuple<int, int, int>;

// ------------
// collatz_eval
// ------------

/**
 * @param t1 tuple of the range
 * @return max cycle length of the range, inclusive
 */
tuple_type_2 collatz_eval (const tuple_type_1& t1);

/**
 * @param val1 the first input value
 * @param val2 the second input value
 * @return max cycle length of the range, inclusive
 */
int solve_collatz(int val1, int val2);

#endif // Collatz_hpp
