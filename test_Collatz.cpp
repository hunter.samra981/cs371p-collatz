// ----------------
// test_Collatz.cpp
// ----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// --------------
// CollatzFixture
// --------------

TEST(CollatzFixture, collatz_eval_0) {
    const tuple_type_1 t1 = {1, 10};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1, 10, 20};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_1) {
    const tuple_type_1 t1 = {100, 200};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {100, 200, 125};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_2) {
    const tuple_type_1 t1 = {201, 210};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {201, 210, 89};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_3) {
    const tuple_type_1 t1 = {900, 1000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {900, 1000, 174};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_4) {
    const tuple_type_1 t1 = {1000, 2000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1000, 2000, 182};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_5) {
    const tuple_type_1 t1 = {1234, 5678};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1234, 5678, 238};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_6) {
    const tuple_type_1 t1 = {123456, 789123};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {123456, 789123, 509};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_7) {
    const tuple_type_1 t1 = {400000, 600000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {400000, 600000, 470};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_8) {
    const tuple_type_1 t1 = {1000, 10000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1000, 10000, 262};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_9) {
    const tuple_type_1 t1 = {100000, 1000000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {100000, 1000000, 525};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_10) {
    const tuple_type_1 t1 = {29345, 109283};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {29345, 109283, 354};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_11) {
    const tuple_type_1 t1 = {9999, 99999};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {9999, 99999, 351};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_12) {
    const tuple_type_1 t1 = {1, 100000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1, 100000, 351};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_13) {
    const tuple_type_1 t1 = {1, 200000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1, 200000, 383};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_14) {
    const tuple_type_1 t1 = {50000, 75000};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {50000, 75000, 340};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_15) {
    const tuple_type_1 t1 = {30, 50};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {30, 50, 110};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_16) {
    const tuple_type_1 t1 = {363, 797};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {363, 797, 171};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_17) {
    const tuple_type_1 t1 = {159, 159};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {159, 159, 55};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_18) {
    const tuple_type_1 t1 = {9, 6};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {9, 6, 20};
    ASSERT_TRUE(t2 == t3);
}

TEST(CollatzFixture, collatz_eval_19) {
    const tuple_type_1 t1 = {1, 2};
    const tuple_type_2 t2 = collatz_eval(t1);
    const tuple_type_2 t3 = {1, 2, 2};
    ASSERT_TRUE(t2 == t3);
}

